package com.ragl.hackerrank.application;

public class Application {

    public static void main (String args[]){
        int a[] = {0,2,3,0,67,0,98};

        for (int i = 0; i < (a.length - 1); i++){
            if ( a[i] == 0) {
                int aux = a[i + 1];
                a[i + 1] = 0;
                a[i] = aux;

                if( i > 0 )
                    if ( a[i - 1] == 0 ){
                        aux = a[i];
                        a[i] = 0;
                        a[i - 1] = aux;
                    }

            }
        }

        for (int x : a)
            System.out.print(x + " ");
    }
}
