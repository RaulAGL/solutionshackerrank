package com.ragl.hackerrank.solutions;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;


public class SolutionReverseDoublyLinkedList {

        static class DoublyLinkedListNode {
            public int data;
            public DoublyLinkedListNode next;
            public DoublyLinkedListNode prev;

            public DoublyLinkedListNode(int nodeData) {
                this.data = nodeData;
                this.next = null;
                this.prev = null;
            }
        }

        static class DoublyLinkedList {
            public DoublyLinkedListNode head;
            public DoublyLinkedListNode tail;

            public DoublyLinkedList() {
                this.head = null;
                this.tail = null;
            }

            public void insertNode(int nodeData) {
                DoublyLinkedListNode node = new DoublyLinkedListNode(nodeData);

                if (this.head == null) {
                    this.head = node;
                } else {
                    this.tail.next = node;
                    node.prev = this.tail;
                }

                this.tail = node;
            }
        }

        public static void printDoublyLinkedList(DoublyLinkedListNode node, String sep, BufferedWriter bufferedWriter) throws IOException {
            while (node != null) {
                System.out.println(node.data);

                node = node.next;

                if (node != null) {
                    bufferedWriter.write(sep);
                }
            }
        }

        // Complete the reverse function below.

        /*
         * For your reference:
         *
         * DoublyLinkedListNode {
         *     int data;
         *     DoublyLinkedListNode next;
         *     DoublyLinkedListNode prev;
         * }
         *
         */
        static DoublyLinkedListNode reverse(DoublyLinkedListNode head) {
            return rev(head);
            // return getHead(head);
        }

        static DoublyLinkedListNode getHead(DoublyLinkedListNode head){
            if(head.next == null)
                return head;
            else
                return getHead(head.next);
        }

        static DoublyLinkedListNode rev(DoublyLinkedListNode head){
            if(head.prev == null && head.next != null){
                head.prev = head.next;
                head.next = null;
                return rev(head.prev);
            } else if (head.prev != null && head.next != null) {
                DoublyLinkedListNode aux = head.prev;
                head.prev = head.next;
                head.next = aux;
                return rev(head.prev);
            } else  {
                head.next = head.prev;
                head.prev = null;
                return head;
            }
        }
        private static final Scanner scanner = new Scanner(System.in);

        public static void main(String[] args) throws IOException {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/raulagl/Documents/response.txt"));

            int t = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int tItr = 0; tItr < t; tItr++) {
                DoublyLinkedList llist = new DoublyLinkedList();

                int llistCount = scanner.nextInt();
                scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

                for (int i = 0; i < llistCount; i++) {
                    int llistItem = scanner.nextInt();
                    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

                    llist.insertNode(llistItem);
                }

                DoublyLinkedListNode llist1 = reverse(llist.head);

                printDoublyLinkedList(llist1, " ", bufferedWriter);
                bufferedWriter.newLine();
            }

            bufferedWriter.close();

            scanner.close();
        }
    }
//   <-   <-   <-
// 1 -> 2 -> 3 -> 4