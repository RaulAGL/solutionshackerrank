package com.ragl.hackerrank.solutions;

import java.util.Scanner;

public class GameArray {

    public static boolean canWin(int leap, int[] game) {
        // Return true if you can win the game; otherwise, return false.
        int position = -1;
        for ( int index = 0; index < game.length; index++ ){
            if ( (index - 1 ) >= 0 && game[index] == 0 ) {
                position = index - 1;
            }
            if ( (index + 1) < game.length ) {
                if ( game[index + 1] == 0 ) {
                    position = index + 1;
                }
            }
            if ( (index + leap) < game.length ) {
                if ( game[index + leap] == 0 )
                    position = index + leap;
            }
            if ( position == (game.length - 1) || (game[index] + leap ) >= game.length ){
                position = game.length;
            }

        }

        return ( position > ( game.length -1 ) );
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int q = scan.nextInt();
        System.out.println( "print q " + q );
        while (q-- > 0) {
            System.out.println("q " + q );
            int n = scan.nextInt();
            int leap = scan.nextInt();
            System.out.println(" n " + n + " leap " + leap  );

            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scan.nextInt();
                System.out.println( "print game[i] " + game[i] );
            }

            for ( int j = 0; j < game.length; j++ )
                System.out.println( "game [" + j + "]" + game[j] );

            System.out.println( (canWin(leap, game)) ? "YES" : "NO" );
        }
        scan.close();
    }
}
