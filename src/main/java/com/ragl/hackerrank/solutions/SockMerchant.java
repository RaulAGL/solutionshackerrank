package com.ragl.hackerrank.solutions;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SockMerchant {

    public static int sockMerchant(int n, int[] ar) {

        int numberOfPairs = 0;
        List pairsList = new ArrayList();
        for(int color : ar){
            if (!pairsList.contains(color)){
                System.out.println(color);
                int numberOfTimes = 0;
                for(int value : ar)
                    if (value == color)
                        numberOfTimes++;
                if (numberOfTimes > 1){
                    numberOfPairs += numberOfTimes / 2;
                }
                pairsList.add(color);
            }

        }
        return numberOfPairs;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void app() throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] ar = new int[n];

        String[] arItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arItem = Integer.parseInt(arItems[i]);
            ar[i] = arItem;
        }

        int result = sockMerchant(n, ar);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
