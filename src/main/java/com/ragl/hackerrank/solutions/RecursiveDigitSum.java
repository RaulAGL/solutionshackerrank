package com.ragl.hackerrank.solutions;


import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

    public class RecursiveDigitSum {

        // Complete the superDigit function below.
        static int superDigit(String n, int k) {

            long plus = 0;
            for (String v : n.split(""))
                plus += Long.parseLong(v);

            plus *= k;

            return superNumber(Long.toString(plus));
        }

        static int superNumber(String n) {
            if(n.length() > 1) {
                Integer plus = 0;
                for (String a : n.split(""))
                    plus += Integer.parseInt(a);

                return superNumber(plus.toString());
            } else
                return Integer.parseInt(n);

        }


        private static final Scanner scanner = new Scanner(System.in);

        public static void main(String[] args) throws IOException {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/raulagl/Documents/response.txt"));

            String[] nk = scanner.nextLine().split(" ");

            String n = nk[0];

            int k = Integer.parseInt(nk[1]);

            int result = superDigit(n, k);
            System.out.println(result);
            bufferedWriter.write(String.valueOf(result));
            bufferedWriter.newLine();

            bufferedWriter.close();

            scanner.close();
        }
    }
