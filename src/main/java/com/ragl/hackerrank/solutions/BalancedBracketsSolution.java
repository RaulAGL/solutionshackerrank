package com.ragl.hackerrank.solutions;


import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.Stack;
import java.util.concurrent.*;
import java.util.regex.*;

public class BalancedBracketsSolution {

    public static class Stack {

        private static List<Character> stack = new ArrayList<Character>();
        private final static List<Character> singsOfEnd = Arrays.asList('}',')',']');
        private final static List<Character> singsOfStart = Arrays.asList('{','(','[');

        public static void add(Character c) {
            stack.add(c);
        }

        public static void remove(Character c) {

            if (stack.size() > 0) {
                int indexLast = stack.size() - 1;
                Character toRemove = stack.get(indexLast);
                if ( c == ')' && toRemove == '(')
                    stack.remove(indexLast);
                else if ( c == '}' && toRemove == '{')
                    stack.remove(indexLast);
                else if ( c == ']' && toRemove == '[')
                    stack.remove(indexLast);
                else
                    stack.add(c);
            } else
                stack.add(c);

        }

        public static String isEmpty(){
            return stack.isEmpty() ? "YES" : "NO";
        }

        public static void print(){
            for (Character c: stack)
                System.out.print(c);
            System.out.println();
        }


    }
    // Complete the isBalanced function below.
    static String isBalanced(String s) {
        char [] sArray = s.toCharArray();

        boolean firstPositionIsClose = Stack.singsOfEnd.contains(sArray[0]);

        if (s.length() % 2 == 1 || firstPositionIsClose)
            return "NO";
        else {
            Stack.stack.clear();
            for (Character c : sArray){
                if (Stack.singsOfEnd.contains(c))
                    Stack.remove(c);
                if (Stack.singsOfStart.contains(c))
                    Stack.add(c);
            }

            return Stack.isEmpty();
        }

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/raulagl/Documents/response.txt"));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            String s = scanner.nextLine();

            String result = isBalanced(s);
            System.out.println(result);
//            bufferedWriter.write(result);
//            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}
