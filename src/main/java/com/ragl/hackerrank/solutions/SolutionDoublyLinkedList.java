package com.ragl.hackerrank.solutions;


import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class SolutionDoublyLinkedList {

    static class DoublyLinkedListNode {
        public int data;
        public DoublyLinkedListNode next;
        public DoublyLinkedListNode prev;

        public DoublyLinkedListNode(int nodeData) {
            this.data = nodeData;
            this.next = null;
            this.prev = null;
        }
    }

    static class DoublyLinkedList {
        public DoublyLinkedListNode head;
        public DoublyLinkedListNode tail;

        public DoublyLinkedList() {
            this.head = null;
            this.tail = null;
        }

        public void insertNode(int nodeData) {
            DoublyLinkedListNode node = new DoublyLinkedListNode(nodeData);

            if (this.head == null) {
                this.head = node;
            } else {
                this.tail.next = node;
                node.prev = this.tail;
            }

            this.tail = node;
        }
    }

    public static void printDoublyLinkedList(DoublyLinkedListNode node, String sep, BufferedWriter bufferedWriter) throws IOException {
        while (node != null) {
            //bufferedWriter.write(String.valueOf(node.data));
            System.out.print(node.data + " ");
            node = node.next;

            if (node != null) {
                bufferedWriter.write(sep);
            }
        }
    }

    // Complete the sortedInsert function below.

    /*
     * For your reference:
     *
     * DoublyLinkedListNode {
     *     int data;
     *     DoublyLinkedListNode next;
     *     DoublyLinkedListNode prev;
     * }
     *
     */

    static DoublyLinkedListNode getHead (DoublyLinkedListNode head){
        if (head.prev != null)
            return getHead(head.prev);
        else
            return head;
    }

    static DoublyLinkedListNode sortedInsert(DoublyLinkedListNode head, int data) {

        head = printList(head,data);

        head = getHead(head);

        return head;
    }
    static DoublyLinkedListNode printList(DoublyLinkedListNode listDoublyLinked, int data){


        if(listDoublyLinked.next != null) {

            DoublyLinkedListNode newNode = new DoublyLinkedListNode(data);

            if (listDoublyLinked.prev != null) {

                if (data <= listDoublyLinked.data  && data >=  listDoublyLinked.prev.data) {

                    newNode.next = listDoublyLinked;
                    newNode.prev = listDoublyLinked.prev;

                    listDoublyLinked.prev.next = newNode;
                    listDoublyLinked.prev = newNode;
                    return newNode;
                }
            } else {

                if (data <= listDoublyLinked.data){

                    newNode.next = listDoublyLinked;
                    listDoublyLinked.prev = newNode;
                    return newNode;
                }
            }

            return printList(listDoublyLinked.next,data);
        } else {

            DoublyLinkedListNode newNode = new DoublyLinkedListNode(data);
            if(data >= listDoublyLinked.data){
                listDoublyLinked.next = newNode;
                newNode.prev = listDoublyLinked;
                return newNode;
            } else {

                listDoublyLinked.prev.next = newNode;
                newNode.next = listDoublyLinked;
                newNode.prev = listDoublyLinked.prev;
                listDoublyLinked.prev = newNode;

                return newNode;
            }


        }

    }
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/raulagl/Documents/response.txt"));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            DoublyLinkedList llist = new DoublyLinkedList();

            int llistCount = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < llistCount; i++) {
                int llistItem = scanner.nextInt();
                scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

                llist.insertNode(llistItem);
            }

            int data = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            DoublyLinkedListNode llist1 = sortedInsert(llist.head, data);

            printDoublyLinkedList(llist1, " ", bufferedWriter);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}
