package com.ragl.hackerrank.solutions;



    import java.util.*;
import java.io.*;

    class Node {
        Node left;
        Node right;
        int data;

        Node(int data) {
            this.data = data;
            left = null;
            right = null;
        }
    }


public class SolutionHeightToTree {
        /*
        class Node
            int data;
            Node left;
            Node right;
        */

        static int getHeigth(Node root,int height){
            if( root != null) {
                if(root.right != null)
                    height = getHeigth(root.right,height);
                if (root.left != null)
                    height = getHeigth(root.left,height);
                height++;
            }

            return height;
        }

        static int height(Node root) {
            // Write your code here.
            if (root != null) {
                if (root.left != null || root.right != null){
                    int sizeRigth = height(root.right);
                    int sizeLeft = height(root.left);
                    return (1 + max(sizeLeft, sizeRigth));
                }
                return 0;
            } else
                return 0;





//            System.out.print("left " + sizeLeft + " rigth " + sizeRigth);
//            System.out.println();

        }

        static int max(int x, int y){
            if (x > y)
                return x;
            else return y;
        }

        static Node insert(Node root, int data) {
            if(root == null) {
                return new Node(data);
            } else {
                Node cur;
                if(data <= root.data) {
                    cur = insert(root.left, data);
                    root.left = cur;
                } else {
                    cur = insert(root.right, data);
                    root.right = cur;
                }
                return root;
            }
        }

        public static void main(String[] args) {
            Scanner scan = new Scanner(System.in);
            int t = scan.nextInt();
            Node root = null;
            while(t-- > 0) {
                int data = scan.nextInt();
                root = insert(root, data);
            }
            scan.close();
            int height = height(root);
            System.out.println(height);
        }

}
