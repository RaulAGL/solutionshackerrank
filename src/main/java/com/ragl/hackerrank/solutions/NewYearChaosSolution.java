package com.ragl.hackerrank.solutions;

import java.util.*;
import java.util.stream.IntStream;


public class NewYearChaosSolution {


    static void minimumBribes(int []q) {
        int[] aInit = IntStream.rangeClosed(1,q.length).toArray();
        int i = 0;
        int numMovs = 0;
        boolean isChaotic = false;
        while( i < q.length - 1) {
            if (aInit[i] != q[i]) {
                /**
                 * busca el valor de aInit[i] en el array q solamente a las dos posiciones siguientes
                 */

                Integer vPlus1 = (i + 1) < q.length ? aInit[i + 1]: null;
                Integer vPlus2 = (i + 2) < q.length ? aInit[i + 2]: null;


                if (vPlus1 != null && vPlus1 == q[i]) {
                    aInit[i + 1] = aInit[i];
                    aInit[i] = vPlus1;
                    numMovs++;
                } else if (vPlus2 != null && vPlus2 == q[i]) {
                    aInit[i + 2] = vPlus1;
                    aInit[i + 1] = aInit[i];
                    aInit[i] = vPlus2;
                    numMovs += 2;
                } else {
                    System.out.println("Too chaotic");
                    isChaotic = true;
                    break;
                }
            }
            i++;
        }

        if (!isChaotic)
            System.out.println(numMovs);

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            int t = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int tItr = 0; tItr < t; tItr++) {
                int n = scanner.nextInt();
                scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

                int[] q = new int[n];

                String[] qItems = scanner.nextLine().split(" ");
                scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

                for (int i = 0; i < n; i++) {
                    int qItem = Integer.parseInt(qItems[i]);
                    q[i] = qItem;
                }

                minimumBribes(q);
            }

            scanner.close();
        } catch (Exception e ){
            System.out.println(" message error " + e.getMessage());
        }
    }
}