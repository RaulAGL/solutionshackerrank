package com.ragl.hackerrank.solutions;

import java.io.*;
import java.util.*;


/*
 * evaluar dos situaciones
 * 1era. cuantos guiones tiene de manera horizontal
 * 2da. cuantos guiones tiene de manera vertical
 * */

public class CrosswordPuzzleSolution {

    // Complete the crosswordPuzzle function below.
    static String[] crosswordPuzzle(String[] crossword, String words) {

        String [] wordsArray = words.split(";");
        int position;
        int index = 0;
        int numberOfHyphenHorizontal;
        int numberOfHyphenVertical;

        while (index < crossword.length) {

            numberOfHyphenHorizontal = 0;
            numberOfHyphenVertical = 0;

            position = crossword[index].indexOf('-');
            if (position > -1) {

                numberOfHyphenHorizontal++;
                numberOfHyphenVertical++;

                // start - evalua numero de guiones medios horizontal
                char [] crosswordOfIndex = crossword[index].toCharArray();

                for(int horizontalIndex = position + 1; horizontalIndex < crosswordOfIndex.length; horizontalIndex++) {
                    if (crosswordOfIndex[horizontalIndex] == '-')
                        numberOfHyphenHorizontal++;
                    else
                        break;
                }

                System.out.println(" numberOfHyphenHorizontal " + numberOfHyphenHorizontal + " from position " + position);
                // end - evalua numero de guiones medios horizontal

                // start - evalua numero de guiones medios vertical

                // end - evalua numero de guiones medios vertical

            }
//            System.out.println(crossword[index]);
            index++;
        }
        return crossword;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/raulagl/Documents/response.txt"));

        String[] crossword = new String[10];

        for (int i = 0; i < 10; i++) {
            String crosswordItem = scanner.nextLine();
            crossword[i] = crosswordItem;
        }

        String words = scanner.nextLine();

        String[] result = crosswordPuzzle(crossword, words);

        for (int i = 0; i < result.length; i++) {
            bufferedWriter.write(result[i]);

            if (i != result.length - 1) {
                bufferedWriter.write("\n");
            }
        }

        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}

