package com.ragl.hackerrank.solutions;

import java.util.Arrays;

public class MinimumSwap2 {


    public static void main(String args[]){

        int arr [] = new int[100000];
        for(int a = 99999; a > 0; a--)
            arr[a] = 100000 - a;



        int swamps = minimumSwaps(arr);

        System.out.println("swamps " + swamps);

    }

    static int minimumSwaps(int[] arr) {

        int copyArr [] = arr.clone();
//        for(int i = 0; i < arr.length; i++)
//            copyArr[i] = arr[i];


//        int [] copyArr = orderArray(array, false);

        Arrays.sort(copyArr);

        printA(copyArr);
        printA(arr);

        int swamp = 0;

        for(int ia = 0; ia < arr.length; ia++){
            System.out.println(arr[ia] + " - " + copyArr[ia] );
            if( arr[ia] != copyArr[ia] ) {
                int pos = searchPosition(copyArr[ia],arr);
                if ( pos >= 0) {
                    int aux = arr[ia];
                    arr[ia] = arr[pos];
                    arr[pos] = aux;
                    swamp++;
                }
            }
        }

        printA(arr);

        return swamp;
    }

    static int searchPosition(int value, int [] arr){

        for(int i = 0; i < arr.length; i++)
            if( arr[i] == value)
                return i;

        return -1;
    }

    static void printA(int [] arr){
        for(int a: arr) System.out.print( a + " ");
        System.out.println("  ");
    }

    static boolean isSorted( int arr [], int arrSorted[] ){

        for(int a = 0; a < arr.length; a++)
            if ( arr[a] != arrSorted[a] )
                return false;

        return true;
    }

    static int [] orderArray(int [] arr, boolean sort) {

        if ( !sort ) {
            sort = true;
            for( int ia = 0; ia < (arr.length - 1); ia++ ) {
                if ( arr[ia] > arr[ia + 1]){
                    sort = false;
                    int aux = arr[ia + 1];
                    arr[ia + 1] = arr[ia];
                    arr[ia] = aux;
                }
            }
            return orderArray(arr, sort);
        }

        return arr;
    }

}
