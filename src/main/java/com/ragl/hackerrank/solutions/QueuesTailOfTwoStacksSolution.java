package com.ragl.hackerrank.solutions;


import java.util.*;

class QueueException extends Exception {

    public QueueException() {
        super();
    }

}

class MyQueue<T> {

    private static class QueueNode<T> {
        private T data;
        private QueueNode<T> next;

        public QueueNode(T data){
            this.data = data;
        }

    }

    private QueueNode<T> first;
    private QueueNode<T> last;

    public void enqueue(T data) {
        QueueNode<T> node = new QueueNode<T>(data);
        if (last != null)
            last.next = node;
        last = node;
        if (first == null)
            first = node;
    }

    public void dequeue() throws QueueException{
        if (first == null)
            throw  new QueueException();

        first = first.next;
    }

    public T peek() throws QueueException {
        if (first == null)
            throw  new QueueException();
        return first.data;
    }

}

public class QueuesTailOfTwoStacksSolution {
    public static void main(String[] args) throws QueueException {


        MyQueue<Integer> queue = new MyQueue<Integer>();

        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        for (int i = 0; i < n; i++) {
            int operation = scan.nextInt();
            if (operation == 1) { // enqueue
                queue.enqueue(scan.nextInt());
            } else if (operation == 2) { // dequeue
                queue.dequeue();
            } else if (operation == 3) { // print/peek
                System.out.println(queue.peek());
            }
        }
        scan.close();
    }
}
