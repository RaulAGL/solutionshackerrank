package com.ragl.hackerrank.solutions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Stack {

    static String [] signosCierreArray = { "}","]",")" };
    static final List signosCierreList = Arrays.asList( signosCierreArray );

    public static void main( String args [] ){
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            String input = sc.next();
            validaCadena( input );
        }


    }

    static void validaCadena( String input ) {

        List<Character> stack = new ArrayList<Character>();
        char key = input.charAt( 0 );
        if ( signosCierreList.contains( Character.toString( key ) ) || ( input.length() % 2 ) == 1 ) {
            System.out.println( false );
        } else {
            stack.add( key );
            for ( int i = 1; i < input.length(); i++ ) {
                key = input.charAt( i );
                boolean status = false;
                int lastPosition = stack.size() - 1;
//                System.out.println( key + " key ");
                if ( signosCierreList.contains( Character.toString( key ) ) && !stack.isEmpty() ) {
//                    System.out.println( "remove " + key );
                    switch ( key ) {
                        case '}' :
                            status = ( stack.get(lastPosition) == '{' );
                            break;
                        case ']' :
                            status = ( stack.get(lastPosition) == '[' );
                            break;
                        case ')' :
                            status = ( stack.get(lastPosition) == '('  );
                            break;
                        default:
                            break;
                    }
                } else {
//                    System.out.println( " add ");
                    stack.add( key );
                }


                if ( status )
                    stack.remove( lastPosition );
            }
//
//            for ( int i = 0; i < stack.size(); i++ )
//                System.out.print( stack.get( i ) );
//
//            System.out.println( "-----" );
//
//            for ( int i = 0; i < signosCierreList.size(); i++ )
//                System.out.print( signosCierreList.get( i ) );


            System.out.println( stack.size() == 0 );
        }
    }

}

